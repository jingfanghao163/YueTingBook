﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler.Models
{
    public class Novel
    {
        public string Book { get; set; }

        public string Chapter { get; set; }

        public string Context { get; set; }


    }
}

